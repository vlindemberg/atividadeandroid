package com.example.vinicius.aulaslayout;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    private static final String[] cidades = new String[] {"Recife", "Olinda", "Caruaru" , "Petrolina",
            "Garanhuns", "Santa Cruz do Capibaribe", "Gravatá", "Belo Jardim",
            "Arcoverde", "Pesqueira", "Bezerros", "Salgueiro",
            "Toritama"};

    private String[] estados = new String[]{"Pernambuco", "Goiás", "São Paulo", "Bahia"};

    private String mSexo;
    private String mEstado;
    private String familia = "";
    private String conhecidos = "";
    private String amigos = "";
    private EditText mName;
    private EditText mEmail;
    private EditText mTelefone;
    private AutoCompleteTextView mAutoCompletCidades;
    private Button mEnviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mName  = (EditText) findViewById(R.id.textviewName);
        mEmail = (EditText) findViewById(R.id.textviewEmail);
        mTelefone  = (EditText) findViewById(R.id.textviewTelefone);

        mAutoCompletCidades = (AutoCompleteTextView) findViewById(R.id.autocompletCidades);
        ArrayAdapter<String> adapterCidades = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, cidades);
        mAutoCompletCidades.setAdapter(adapterCidades);

        final Spinner spinnerEstados = (Spinner) findViewById(R.id.spinnerEstados);
        ArrayAdapter<String> adapterEstados = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, estados);
        adapterEstados.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spinnerEstados.setAdapter(adapterEstados);
        spinnerEstados.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mEstado  = estados[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radio_group_sexo);
        assert (radioGroup != null);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(group.getCheckedRadioButtonId());
                mSexo = rb.getText().toString();
            }
        });

        CheckBox checkBoxFamilia = (CheckBox) findViewById(R.id.checkboxF);
        CheckBox checkBoxAmigos = (CheckBox) findViewById(R.id.checkboxA);
        CheckBox checkBoxConhecidos = (CheckBox) findViewById(R.id.checkboxC);

        assert (checkBoxFamilia != null);
        assert (checkBoxAmigos != null);
        assert (checkBoxConhecidos != null);

        checkBoxFamilia.setOnCheckedChangeListener(MainActivity.this);
        checkBoxAmigos.setOnCheckedChangeListener(MainActivity.this);
        checkBoxConhecidos.setOnCheckedChangeListener(MainActivity.this);

        mEnviar = (Button) findViewById(R.id.button_enviar);
        mEnviar.setOnClickListener(this);

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        switch (buttonView.getId()){
            case R.id.checkboxA:
                amigos = (buttonView.isChecked() ? buttonView.getText().toString() : "");
                break;
            case R.id.checkboxC :
                conhecidos = (buttonView.isChecked() ? buttonView.getText().toString() : "");
                break;
            case R.id.checkboxF :
                familia = (buttonView.isChecked() ? buttonView.getText().toString() : "");
                break;
        }
    }

    @Override
    public void onClick(View v) {


        String nome = mName.getText().toString();
        String email = mEmail.getText().toString();
        String telefone = mTelefone.getText().toString();
        String cidade = mAutoCompletCidades.getText().toString();

        Intent intent = new Intent(this, ListActivity.class );

        intent.putExtra("nome",nome);
        intent.putExtra("email",email);
        intent.putExtra("telefone",telefone);
        intent.putExtra("cidade",cidade);
        intent.putExtra("estado",mEstado);
        intent.putExtra("sexo",mSexo);
        intent.putExtra("parentesco",amigos+" "+conhecidos+" "+familia);

        startActivity(intent);

    }
}
