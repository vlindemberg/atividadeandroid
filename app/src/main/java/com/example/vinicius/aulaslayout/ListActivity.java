package com.example.vinicius.aulaslayout;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by Vinicius on 14/09/2016.
 */
public class ListActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        Intent intent = getIntent();

        TextView nome = (TextView) findViewById(R.id.nome);
        nome.setText(intent.getStringExtra("nome"));

        TextView email = (TextView) findViewById(R.id.email);
        email.setText(intent.getStringExtra("email"));

        TextView telefone = (TextView) findViewById(R.id.telefone);
        telefone.setText(intent.getStringExtra("telefone"));

        TextView cidade = (TextView) findViewById(R.id.cidade);
        cidade.setText(intent.getStringExtra("cidade"));

        TextView estado = (TextView) findViewById(R.id.estado);
        estado.setText(intent.getStringExtra("estado"));

        TextView sexo = (TextView) findViewById(R.id.sexo);
        sexo.setText(intent.getStringExtra("sexo"));

        TextView parentesco = (TextView) findViewById(R.id.parentesco);
        parentesco.setText(intent.getStringExtra("parentesco"));

    }
}
